#!/usr/bin/python3

import csv
import numpy

voltages = []


with open('scope_0.txt') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = int(1)
    for row in csv_reader:
        if (row[1] == "Volt"):
            continue
        voltages.append(float(row[1]))
        line_count += 1
    print("Read {0} lines".format(line_count))

squares = numpy.float_power(voltages,2)
average = numpy.average(squares)
root = numpy.sqrt(average)
print("The RMS Value is {0:.3f}".format(root))